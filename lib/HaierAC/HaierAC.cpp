#include "HaierAC.h"
#include <Arduino.h>

void HaierAC::init() {
  byte initialization_1[13] = {0xFF, 0xFF, 0x0A, 0x0,  0x0,  0x0, 0x0,
                               0x0,  0x00, 0x61, 0x00, 0x07, 0x72};
  byte initialization_2[13] = {0xFF, 0xFF, 0x08, 0x40, 0x0,  0x0, 0x0,
                               0x0,  0x0,  0x70, 0xB8, 0x86, 0x41};

  delay(1000);
  Serial.write(initialization_1, sizeof(initialization_1));
  delay(1000);
  Serial.write(initialization_2, sizeof(initialization_2));
}

bool HaierAC::setTemp(uint8_t temp) {
  if (temp >= MIN_TEMP && temp <= MAX_TEMP) {
    this->temp = temp;
    this->command[TEMP_BYTE] = temp - MIN_TEMP;
  }

  return sendCommand(this->command, sizeof(this->command));
}

bool HaierAC::setFan(fan_modes fan) {
  this->fan = fan;
  this->command[FAN_BYTE] &= 0xF0;

  switch (fan) {
  case F_LOW:
    this->command[FAN_BYTE] |= 0x03;
    break;
  case F_MID:
    this->command[FAN_BYTE] |= 0x02;
    break;
  case F_HIGH:
    this->command[FAN_BYTE] |= 0x01;
    break;
  case F_AUTO:
    this->command[FAN_BYTE] |= 0x05;
    break;
  }

  return sendCommand(this->command, sizeof(this->command));
}

bool HaierAC::setMode(modes mode) {
  this->mode = mode;
  byte newMode = 0;

  switch (mode) {
  case M_HEAT:
    newMode = 0x80;
    break;
  case M_COOL:
    newMode = 0x20;
    break;
  case M_DRY:
    newMode = 0x40;
    break;
  case M_FAN:
    if (this->fan == F_AUTO) {
      setFan(F_LOW);
      delay(1000);
    }
    newMode = 0xC0;
    break;
  case M_OFF:
    return sendCommand(this->power_command, sizeof(this->power_command));
  }

  this->command[MODE_BYTE] &= 0x0F;
  this->command[MODE_BYTE] |= newMode;

  return sendCommand(this->command, sizeof(this->command));
}

bool HaierAC::setSwing(swing_modes swing) {
  this->swing = swing;
  switch (swing) {
  case S_AUTO:
    this->command[SWING_BYTE] = 0x0C;
    break;
  case S_CENTER:
    this->command[SWING_BYTE] = 0x06;
    break;
  }

  return sendCommand(this->command, sizeof(this->command));
}

uint8_t HaierAC::getTemp() { return this->temp; }

uint8_t HaierAC::getCurrTemp() { return this->currTemp; }

fan_modes HaierAC::getFan() { return this->fan; }

modes HaierAC::getMode() { return this->mode; }

swing_modes HaierAC::getSwing() { return this->swing; }

bool HaierAC::getData() {
  if ((this->lastPoll + this->pollingRate) < millis()) {
    Serial.write(this->poll, sizeof(this->poll));
    this->lastPoll = millis();
  }

  byte data[47];
  if (Serial.available() > 0) {
    if ((data[0] = Serial.read()) != 0xFF)
      return false;
    if ((data[1] = Serial.read()) != 0xFF)
      return false;

    Serial.readBytes(data + 2, sizeof(data) - 2);

    if (data[9] == 2) {
      memcpy(this->status, data, sizeof(this->status));
      return processData();
    } else {
      return false;
    }
  } else {
    return false;
  }
}

bool HaierAC::sendCommand(const byte *message, byte size) {
  byte crc_offset = (2 + command[2]);
  byte crc = getChecksum(command, sizeof(command));
  word crc_16 = crc16(0, &(command[2]), crc_offset - 2);

  // Updates the crc
  command[crc_offset] = crc;
  command[crc_offset + 1] = (crc_16 >> 8) & 0xFF;
  command[crc_offset + 2] = crc_16 & 0xFF;

  if (Serial.write(message, size)) {
    return true;
  } else {
    return false;
  }
}

bool HaierAC::processData() {
  byte check = getChecksum(this->status, sizeof(this->status));

  if (check != this->status[2 + this->status[2]]) {
    return false;
  }

  if ((this->status[17] & 0x02) == 0x02) {
    this->purify = true;
  } else {
    this->purify = false;
  }
  // Obecna temperatura
  this->currTemp = this->status[CURR_TEMP_BYTE] / 2;

  // Docelowa temperatura
  this->temp = this->status[TEMP_BYTE] + MIN_TEMP;

  byte statusByte;

  // Tryb
  statusByte = this->status[MODE_BYTE] & 0xF0;

  switch (statusByte) {
  case 0x80:
    this->mode = M_HEAT;
    break;
  case 0x40:
    this->mode = M_DRY;
    break;
  case 0xC0:
    this->mode = M_FAN;
    break;
  default:
    this->mode = M_COOL;
    break;
  }

  if ((this->status[STATUS_BYTE] & 0x01) == 0x00) {
    this->mode = M_OFF;
  }

  // Prędkość wentylatora
  statusByte = this->status[FAN_BYTE] & 0x0F;

  switch (statusByte) {
  case 0x03:
    this->fan = F_LOW;
    break;
  case 0x02:
    this->fan = F_MID;
    break;
  case 0x01:
    this->fan = F_HIGH;
    break;
  default:
    this->fan = F_AUTO;
    break;
  }

  // Kierunek nawiewu
  statusByte = this->status[SWING_BYTE];

  switch (statusByte) {
  case 0x0C:
    this->swing = S_AUTO;
    break;
  default:
    this->swing = S_CENTER;
    break;
  }

  return true;
}

byte HaierAC::getChecksum(const byte *message, size_t size) {
  byte position = (2 + message[2]);
  byte crc = 0;

  if (size < (position)) {
    return 0;
  }

  for (int i = 2; i < position; i++) {
    crc += message[i];
  };

  return crc;
}

unsigned HaierAC::crc16(unsigned crc, unsigned char *buf, size_t len) {
  while (len--) {
    crc ^= *buf++;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
    crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
  }
  return crc;
}
