#ifndef HaierAC_h
#define HaierAC_h

#include <Arduino.h>

#define MODE_BYTE 14
#define FAN_BYTE 14
#define SWING_BYTE 13
#define TEMP_BYTE 12
#define STATUS_BYTE 17
#define CURR_TEMP_BYTE 22

#define MIN_TEMP 16
#define MAX_TEMP 30

enum modes {M_OFF, M_HEAT, M_COOL, M_DRY, M_FAN};
enum fan_modes {F_LOW, F_MID, F_HIGH, F_AUTO};
enum swing_modes {S_AUTO, S_CENTER};


class HaierAC {
    private:
        modes mode = M_HEAT;
        fan_modes fan = F_AUTO;
        swing_modes swing = S_CENTER;
        uint8_t temp = 22;
        uint8_t currTemp = 22;
        unsigned long lastPoll = 0;
        bool purify = true;
        int pollingRate = 10000;

        byte status[47];
        byte command[25] = {0xFF, 0xFF, 0x14, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
                            0x01, 0x60, 0x01, 0x09, 0x08, 0x25, 0x00, 0x02, 0x01,
                            0x00, 0x06, 0x00, 0x00, 0x03, 0x0B, 0x70};
        const byte power_command[17] = {0xFF, 0xFF, 0x0C, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
                                  0x01, 0x5D, 0x01, 0x00, 0x00, 0xAB, 0x7D, 0x3A};
        const byte poll[15] = {0xFF, 0xFF, 0x0A, 0x40, 0x00, 0x00, 0x00, 0x00,
                               0x00, 0x01, 0x4D, 0x01, 0x99, 0xB3, 0xB4};

        bool processData();
        byte getChecksum(const byte * message, size_t size);
        unsigned crc16(unsigned crc, unsigned char *buf, size_t len);
    public:
        void init();

        bool setTemp(uint8_t temp);
        bool setFan(fan_modes fan);
        bool setMode(modes mode);
        bool setSwing(swing_modes swing);

        uint8_t getTemp();
        uint8_t getCurrTemp();
        fan_modes getFan();
        modes getMode();
        swing_modes getSwing();

        bool getData();
        bool sendCommand(const byte *message, byte size);
};

#endif
