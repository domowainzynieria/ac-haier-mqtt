#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "HaierAC.h"

#define AC_NAME "Warsztat klimatyzacja"
#define AC_MANUF "Haier"
#define AC_MODEL "AS35TADHRA-CLC"

#define MQTT_TOPIC_PREFIX "/garaz/warsztat/ac"

#define MQTT_SERVER "MQTT ADDRESS"
#define MQTT_PORT 1883
#define MQTT_CLIENT_NAME "MQTT CLIENT NAME"
#define MQTT_USERNAME "MQTT USERNAME"
#define MQTT_PASS "MQTT PASSWORD"
#define MQTT_ONLINE_TOPIC "/garaz/warsztat/ac/online"
#define MQTT_ONLINE_MESSAGE "online"
#define MQTT_OFFLINE_MESSAGE "offline"

#define WIFI_SSID "WIFI SSID"
#define WIFI_PASS "WIFI PASSWORD"

#define RECONNECT_INTERVAL 180000 // ms

#define MQTT_BUFFER 2048

WiFiClient wifi = WiFiClient();
PubSubClient mqtt = PubSubClient(wifi);
HaierAC ac = HaierAC();

bool MQTTConnected = false;
unsigned long reconnectTimeout = 0;

bool WiFiConnect();
bool MQTTConnect();
bool MQTTSubscribe();
bool MQTTPublish(const char *topic, const char *payload, bool retained);
void MQTTcallback(char *, byte *, unsigned int);
bool sendUpdate();
bool sendHAssDiscovery();

void setup() {
  mqtt.setServer(MQTT_SERVER, MQTT_PORT);
  mqtt.setBufferSize(MQTT_BUFFER);
  mqtt.setCallback(MQTTcallback);

  MQTTConnect();

  Serial.begin(9600);
  delay(1000);

  ac.init();

  delay(1000);
}

void loop() {
  // Maintain MQTT connection
  if(MQTTConnected) {
    if(!(mqtt.loop())) {
      MQTTConnected = false;
      MQTTConnect();
    }
  } else {
    MQTTConnect();
  }

  // Fetch current AC settings
  if(ac.getData()) {
    if(MQTTConnected) sendUpdate();
  }

  delay(50);
}

/* Connect to WiFi */
bool WiFiConnect() {
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(WIFI_SSID, WIFI_PASS);

    uint8_t i = 0;
    while (WiFi.status() != WL_CONNECTED) {
      if ((WiFi.status() == WL_CONNECT_FAILED) || (i > 20)) {
        return false;
      }
      delay(500);
      i++;
    }

    return true;
  } else {
    return true;
  }
}

/* Connect to MQTT server */
bool MQTTConnect() {
  if (millis() > reconnectTimeout) {
    if (WiFiConnect()) {
      if (mqtt.connect(MQTT_CLIENT_NAME, MQTT_USERNAME, MQTT_PASS,
                       MQTT_ONLINE_TOPIC, 0, 1, MQTT_OFFLINE_MESSAGE)) {
        mqtt.publish(MQTT_ONLINE_TOPIC, MQTT_ONLINE_MESSAGE, true);
        MQTTConnected = true;
        MQTTSubscribe();
        sendHAssDiscovery();
        return true;
      } else {
        reconnectTimeout = millis() + RECONNECT_INTERVAL;
        MQTTConnected = false;
        return false;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
}

/* Subscribe MQTT topics */
bool MQTTSubscribe() {
  if (
    mqtt.subscribe((String(MQTT_TOPIC_PREFIX) + "/fanMode/set").c_str()) &&
    mqtt.subscribe((String(MQTT_TOPIC_PREFIX) + "/mode/set").c_str()) &&
    mqtt.subscribe((String(MQTT_TOPIC_PREFIX) + "/swingMode/set").c_str()) &&
    mqtt.subscribe((String(MQTT_TOPIC_PREFIX) + "/temp/set").c_str())
  ) {
    return true;
  } else {
    return false;
  }
}

/* Publish MQTT message with checking connection to the server */
bool MQTTPublish(const char *topic, const char *payload, bool retained) {
  if (sizeof(payload) <= MQTT_BUFFER) {
    if (MQTTConnected) {
      if(mqtt.publish(topic, payload, retained)) {
        return true;
      }else {
        MQTTConnected = false;
      }
    }

    if (MQTTConnect()) {
     return mqtt.publish(topic, payload, retained);
    }else {
      return false;
    }
  } else {
    return false;
  }
}

/* Send current status of the AC through MQTT */
bool sendUpdate() {
  StaticJsonDocument<512> updateBuffer;
  String updateMessage;

  // Current temperature
  updateBuffer["currTemp"] = ac.getCurrTemp();

  // Fan speed
  switch (ac.getFan()) {
  case F_AUTO:
    updateBuffer["fanMode"] = "auto";
    break;
  case F_LOW:
    updateBuffer["fanMode"] = "low";
    break;
  case F_MID:
    updateBuffer["fanMode"] = "medium";
    break;
  case F_HIGH:
    updateBuffer["fanMode"] = "high";
    break;
  }

  // Mode
  switch (ac.getMode()) {
  case M_COOL:
    updateBuffer["mode"] = "cool";
    break;
  case M_HEAT:
    updateBuffer["mode"] = "heat";
    break;
  case M_FAN:
    updateBuffer["mode"] = "fan_only";
    break;
  case M_DRY:
    updateBuffer["mode"] = "dry";
    break;
  case M_OFF:
    updateBuffer["mode"] = "off";
    break;
  }

  // Swing
  switch (ac.getSwing()) {
  case S_AUTO:
    updateBuffer["swingMode"] = "auto";
    break;
  case S_CENTER:
    updateBuffer["swingMode"] = "center";
    break;
  };

  // Goal temperature
  updateBuffer["temp"] = ac.getTemp();

  serializeJson(updateBuffer, updateMessage);
  return MQTTPublish((String(MQTT_TOPIC_PREFIX) + "/state").c_str(),
                      updateMessage.c_str(), true);
}

void MQTTcallback(char *topic, byte *payload, unsigned int length) {
  uint8_t fullTopicLength = strlen(topic);
  char fullTopic[fullTopicLength + 1];
  strncpy(fullTopic, topic, fullTopicLength);
  fullTopic[fullTopicLength] = '\0';

  char message[length + 1];
  strncpy(message, (char*)payload, length);
  message[length] = '\0';

  if (strstr(fullTopic, "/fanMode/set") != NULL) {
    if (strstr(message, "auto")) {
      ac.setFan(F_AUTO);
    } else if (strstr(message, "low")) {
      ac.setFan(F_LOW);
    } else if (strstr(message, "medium")) {
      ac.setFan(F_MID);
    } else if (strstr(message, "high")) {
      ac.setFan(F_HIGH);
    }
  } else if (strstr(fullTopic, "/mode/set") != NULL) {
    if (strstr(message, "cool")) {
      ac.setMode(M_COOL);
    } else if (strstr(message, "heat")) {
      ac.setMode(M_HEAT);
    } else if (strstr(message, "fan_only")) {
      ac.setMode(M_FAN);
    } else if (strstr(message, "dry")) {
      ac.setMode(M_DRY);
    } else if (strstr(message, "off")) {
      ac.setMode(M_OFF);
    }
  } else if (strstr(fullTopic, "/swingMode/set") != NULL) {
    if (strstr(message, "auto")) {
      ac.setSwing(S_AUTO);
    } else if (strstr(message, "center")) {
      ac.setSwing(S_CENTER);
    }
  } else if (strstr(fullTopic, "/temp/set") != NULL) {
    ac.setTemp(atoi(message));
  }

  sendUpdate();
}

/* Send Home Assistant MQTT Discovery message */
bool sendHAssDiscovery() {
    String nameString = String(AC_NAME);
    nameString.replace(" ", "");
    String topicString = String(MQTT_TOPIC_PREFIX);
    String chipId = String(ESP.getChipId());
    StaticJsonDocument<900> confBuffer;
    String confHVAC;

    confBuffer["name"] = nullptr;
    confBuffer["uniq_id"] = nameString + chipId;                    //unique_id
    confBuffer["dev"]["ids"] = nameString + chipId;                 //device identifiers
    confBuffer["dev"]["name"] = AC_NAME;                         //device name
    confBuffer["dev"]["sw"] = "1.0";                                //device sw_version
    confBuffer["dev"]["mdl"] = AC_MODEL;                         //device model
    confBuffer["dev"]["mf"] = AC_MANUF;                          //device manufacturer
    confBuffer["avty_t"] = MQTT_ONLINE_TOPIC;                 //availability_topic

    confBuffer["curr_temp_t"] = topicString + "/state";             //current_temperature_topic
    confBuffer["curr_temp_tpl"] = "{{ value_json.currTemp }}";      //current_temperature_template

    confBuffer["fan_mode_stat_t"] = topicString + "/state";         //fan_mode_state_topic
    confBuffer["fan_mode_stat_tpl"] = "{{ value_json.fanMode }}";   //fan_mode_state_template
    confBuffer["fan_mode_cmd_t"] = topicString + "/fanMode/set";    //fan_mode_command_topic

    confBuffer["max_temp"] = 30;
    confBuffer["min_temp"] = 16;

    confBuffer["mode_stat_t"] = topicString + "/state";             //mode_state_topic
    confBuffer["mode_stat_tpl"] = "{{ value_json.mode }}";          //mode_state_template
    confBuffer["mode_cmd_t"] = topicString + "/mode/set";           //mode_command_topic
    confBuffer["modes"].add("off");
    confBuffer["modes"].add("heat");
    confBuffer["modes"].add("cool");
    confBuffer["modes"].add("dry");
    confBuffer["modes"].add("fan_only");

    confBuffer["swing_mode_stat_t"] = topicString + "/state";           //swing_mode_state_topic
    confBuffer["swing_mode_stat_tpl"] = "{{ value_json.swingMode }}";   //swing_mode_state_template
    confBuffer["swing_modes"].add("auto");
    confBuffer["swing_modes"].add("center");
    confBuffer["swing_mode_cmd_t"] = topicString + "/swingMode/set";    //swing_mode_command_topic

    confBuffer["temp_stat_t"] = topicString + "/state";             //temperature_state_topic
    confBuffer["temp_stat_tpl"] = "{{ value_json.temp }}";     //temperature_state_template
    confBuffer["temp_cmd_t"] = topicString + "/temp/set";           //temperature_command_topic

    confBuffer["temp_step"] = 1;

    serializeJson(confBuffer, confHVAC);
    return MQTTPublish(("homeassistant/climate/" + nameString + chipId + "/config").c_str(), confHVAC.c_str(), true);
}
